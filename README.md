# RoboCode Solutis
Após analisar o comportamento dos robôs existentes no robocode, decidi formular uma estratégia que fosse capaz de me manter vivo tempo suficiente para conseguir uma boa colocação. Para isso, tive a ideia de fazer com que meu robô se movesse da forma mais aleatória possível com o intuito de evitar o máximo de tiros, no entanto, ao se movimentar de forma aleatória, ele perdia muita vida em colisões com a parede. Assim, para evitar colisões, desenvolvi um robô que lê as dimensões da arena, cria uma margem nela e, toda vez que ultrapassar essa margem, faz um retorno evitando a tão indesejada colisão (evita 90%). 

Mas, como nem tudo são flores, a estratégia se mostrou poco eficiente pois o robô, ao chegar à margem, tinha um padrão pre estabelecido de comportamento, fazendo com que sua movimentação ficasse simples (não evitava tiro) e entrasse num loop. Depois de pensar um poco resolvi estabelecer um pouco de inteligência para o robô. A priori, minha ideia era fazer o robô escolher a direção que iria contornar conforme sua posição na arena. Para que isso fosse possível, dividi o espaço em 4 quadrantes (como o plano cartesiano) e programei o robô para se comportar de maneira diferente em cada um deles. Dessa forma, ele vira para o lugar certo toda vez que ultrapassa a margem. 

As flores ainda não são tudo. Com decorrer de mais testes percebi que minha estratégia era muito eficiente quando tinham poucos robôs na arena, e pouco eficiente quando tinham muitos. Como não sei com quantos irei competir agreguei um padrão duplo de comportamento que faz com que meu robô fique recolhido em um movimento quadrático enquanto a arena está cheia. 

 

Quanto ao mais, decidi programar um robô original, utilizando estratégias que eu criei. 

 
 

Ponto forte: Capacidade de se adaptar ao ambiente de batalha.
Ponto fraco: pouco poder de ataque.


