package robo01;

import robocode.*;

public class basico extends AdvancedRobot
{
	
	public void run() {	
		
		double pontox = getX();
		double pontoy = getY();
		 double altura = getBattleFieldHeight();
		double largura = getBattleFieldWidth();
		double angulo = getHeading();
		int quantidade = getOthers();
		
		while(true) {
			quantidade = getOthers();
			setAdjustGunForRobotTurn(true);   
			
			
			if (quantidade <= 4){
				
				while(true) {
					 
					if (pontox >= 150 & pontox <= largura - 150 & pontoy > 150 & pontoy < altura - 150) 
					{
				
						ahead(100);
						setTurnGunLeft(360);;
						pontox = getX();
						pontoy = getY();
						
			     	}
					
					else {
						
						if (angulo > 90 & angulo < 270 & pontox < largura/2 & pontoy > altura/2){turnLeft(90); ahead(100);}        // segundo quadrante
						else {if (angulo > 90 & angulo < 270 & pontox < largura/2 & pontoy < altura/2){turnRight(90); ahead(100);} // quarto quadrante
						else {if (angulo < 90 & angulo > 270 & pontox > largura/2 & pontoy > altura/2){turnRight(90); ahead(100);} // primeiro  
						else {if (angulo < 90 & angulo > 270 & pontox < largura/2 & pontoy < altura/2){turnLeft(90); ahead(100);}  //terceiro   
						else {turnRight(90); ahead(100);}}}}
						setTurnGunLeft(360);
						pontox = getX();
						pontoy = getY();
				         }
			          }
                   } 	
			
			  else{	
					 
			       while (true) {
			    	   
			    	 	if (pontox >= 90 & pontox <= largura - 90 & pontoy >= 90 & pontoy <= altura - 90)
			    	 		{	
			    	 		ahead(100);
			    	 		turnLeft(100);
			    	 		turnGunLeft(360);
			    	 		pontox = getX();
			    	 		pontoy = getY();
			    	 		quantidade = getOthers();
			    	 		if (quantidade <= 4) {break;};
			    	 		}
			    	 	
					     else {
					    	turnRight(45);
					    	back(100);
							pontox = getX();
							pontoy = getY();
				    	 	quantidade = getOthers();
				    	    if (quantidade <= 4) {break;}; 			
			    }
			  }
			}		
		 }
      }		

	
	
	public void onScannedRobot(ScannedRobotEvent e) {
		
		double anguloab = e.getBearingRadians() + getHeadingRadians();
		double turno = anguloab - getGunHeadingRadians();
		setTurnGunRightRadians(robocode.util.Utils.normalRelativeAngle(turno)); 
		fire(1);	

	}

	public void onHitByBullet(HitByBulletEvent e) {
		
					 			
	}
	
	
	public void onHitWall(HitWallEvent e) {
		turnRight(60);
		back(100); 
			
	}	
}
